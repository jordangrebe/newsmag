<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
$dotenv = Dotenv\Dotenv::createImmutable('../');
$dotenv->load();

define('CDG_AUTHOR', 'Jordan Grébé');
define('CDG_NAME', 'Coddyger');
define('CDG_VERSION', '2.0.1');
define('CDG_YEAR', '2022.10.21');
/* ************************** */
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('API_SECRET', $_ENV['API_SECRET']);
define('JWT_SECRET', $_ENV['JWT_SECRET']);
/*
|--------------------------------------------------------------------------
| SYSTEM DIRECTORY AND PATHS
|--------------------------------------------------------------------------
|
| SYS_DIR :: Main system's directory, contains all files and folder of whole Coddyger
| APP_DIR :: Main application's directory, contains all files and folder of application
|
*/
define('SYS_DIR', 'core');
define('APP_DIR', 'app');
/*
|--------------------------------------------------------------------------
| SYSTEM VARIABLES
|--------------------------------------------------------------------------
|
*/
define('CDG_ENV', $_ENV['CDG_ENV']);
define('CDG_APPNAME', $_ENV['CDG_APPNAME']);
define('CDG_HOMEPAGE', $_ENV['CDG_HOMEPAGE']);
define('CDG_PROTOCOL', $_ENV['CDG_PROTOCOL']);
define('CDG_STATUS', $_ENV['CDG_STATUS']);
/*
|--------------------------------------------------------------------------
| APPLICATION DIRECTORY AND PATHS
|--------------------------------------------------------------------------
| VIEWS :: Application views path, contains all view controllers of generated packages
| VENDORS :: Application vendor path, contains all none owned and/or owned javascript plugins
| DATA :: Application data path, contains all media (audio, image, document and all other type of media) uploaded by users
|
*/
define('BASEURL', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST']) . '/';

define('DATABASES', SYS_DIR . '/database');
define('ROUTES', APP_DIR . '/routes');
    define('API', ROUTES . '/api');
    define('WEB', ROUTES . '/web');
define('CONTROLLERS', APP_DIR . '/controllers');
define('MIDDLEWARES', APP_DIR . '/middlewares');
    
define('HELPERS', APP_DIR . '/helpers');
define('MODELS', APP_DIR . '/models');
define('PUBLICS', APP_DIR . '/public');
    define('DATA', PUBLICS . '/data');
define('VIEWS', APP_DIR . '/views');

define('DS', '/');

/*
|--------------------------------------------------------------------------
| MySQL VARIABLES
|--------------------------------------------------------------------------
|
*/
define('DBHOST', $_ENV['DBHOST']);
define('DBNAME', $_ENV['DBNAME']);
define('DBUSER', $_ENV['DBUSER']);
define('DBPASS', $_ENV['DBPASS']);


/*
|--------------------------------------------------------------------------
| MONGODB VARIABLES
|--------------------------------------------------------------------------
|
*/
define('MONGODB_CLUSTER', $_ENV['MONGODB_CLUSTER']);
define('MONGODB_DATABASE', $_ENV['MONGODB_DATABASE']);
define('MONGODB_USER', $_ENV['MONGODB_USER']);
define('MONGODB_PASS', $_ENV['MONGODB_PASS']);

/*
|--------------------------------------------------------------------------
| SOCIAL NETWORK LINKS
|--------------------------------------------------------------------------
|
*/
define('SC_FACEBOOK', $_ENV['SC_FACEBOOK']);
define('SC_GOOGLE', $_ENV['SC_GOOGLE']);
define('SC_TWITTER', $_ENV['SC_TWITTER']);
define('SC_WHATSAPP', $_ENV['SC_WHATSAPP']);
define('SC_INSTAGRAM', $_ENV['SC_INSTAGRAM']);
