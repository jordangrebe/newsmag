<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
namespace Coddyger;

class cdg_protocol
{
    public static function init_secure_connection()
    {
        if(CDG_PROTOCOL && (empty($_SERVER["HTTPS"]) || $_SERVER['HTTPS'] == 'off')){
            self::Redirection("https://" . $_SERVER['HTTP_HOST'] . "/");
        }
        return 0;
    }
    private static function Redirection($link){
        header("Location: $link");
    }
}