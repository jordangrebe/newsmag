<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
namespace Coddyger;

class cdg_datetime
{
    private $local = 'en';
    private $timestamp;
    private $date_format;
    private $date_sets = array('%A', '%d', '%B', '%Y');

    public function __construct($set_local = 'en'){
        $this->local = $set_local;

        \setlocale(LC_TIME, ucwords($this->local));
    }
    public function init_date_format($set_timestamp, $available_sets = ''){
        $this->timestamp = (is_numeric($set_timestamp) ? $set_timestamp : strtotime($set_timestamp));

        $sets = array();
        if(preg_match("#d|m|y|dn#", $available_sets)){
            if(strpos($available_sets, 'dn') !== false){
                $sets[] = '%A';
            }
            if(strpos($available_sets, 'd') !== false){
                $sets[] = '%d';
            }
            if(strpos($available_sets, 'm') !== false){
                $sets[] = '%B';
            }
            if(strpos($available_sets, 'y') !== false){
                $sets[] = '%Y';
            }
        }else{
            $sets[] = '%A %d %B %Y';
        }
        $this->date_format = strftime(implode(' ', $sets), $this->timestamp);

        return utf8_encode($this->date_format);
    }
    public static function init_month_list(){
        $months = array(1 => 'Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
        return $months;
    }
    public static function time_elapsed_string($datetime, $full = false) {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'an',
            'm' => 'mois',
            'w' => 'semaine',
            'd' => 'jour',
            'h' => 'heure',
            'i' => 'minute',
            's' => 'seconde',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? 'il y a ' . implode(', ', $string) : 'à l\'instant';
    }
    public static function time_format($datetime){
        return date("H:i",strtotime($datetime));
    }
}