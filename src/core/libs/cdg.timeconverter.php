<?php
/*
|--------------------------------------------
| TimeConverter
|--------------------------------------------
| Author :: Jordan Grébé
| All units, values and formulas are from Google.com search result
|
*/
namespace Coddyger;

class cdg_timeconverter
{
    public function init_time_converter($value, $unit_from, $unit_to){
        if(!array_key_exists($unit_from, $this->units_sets)){ // Checking for source unit existence in units_sets array
            die('time_converter::source unit not found.');
        }elseif(!array_key_exists($unit_to, $this->units_sets[$unit_from])){ // Checking for destination unit existence in units_sets array
            die('time_converter::destination unit not found.');
        }elseif(array_key_exists($unit_from, $this->units_sets) && array_key_exists($unit_to, $this->units_sets[$unit_from])){
            return $this->_init_converter_($value, $unit_from, $unit_to); // Initialize conversion on units existence
        }else{
            die('time_converter::units not found.');
        }
    }
    private function _init_converter_($value, $from, $to){
        $from_unit_index = array_search($this->units_sets[$from][$from], array_values($this->units_sets[$from])); // Getting source unit index
        $to_unit_index = array_search($this->units_sets[$from][$to], array_values($this->units_sets[$from])); // Getting destination unit index

        $to_unit_value = $this->units_sets[$from][$to]; // Getting destination unit key value from units_sets array
        if($from_unit_index > $to_unit_index){
            // Return multiplication if source index value is larger than destination index value
            return $value * $to_unit_value;
        }elseif($from_unit_index < $to_unit_index){
            // Return division if source index value is smaller than destination index value
            return $value / $to_unit_value;
        }elseif($from_unit_index = $to_unit_index){
            // Return parameter $value if source index value is equal to destination index value
            return $value;
        }else{
            // undefined index range
            return 'time_converter::undefined index range.';
        }
    }
    private $units_sets = array(
        'nanosecond' => array(
            'nanosecond' => 1,
            'microsecond' => 1000,
            'millisecond' => 1e+6,
            'second' => 1e+9,
            'minute' => 6e+10,
            'hour' => 3.6e+12,
            'day' => 8.64e+13,
            'week' => 6.048e+14,
            'month' => 2.628e+15,
            'year' => 3.154e+16,
            'decade' => 3.154e+17,
            'century' => 3.154e+18,
        ),
        'microsecond' => array(
            'nanosecond' => 1000,
            'microsecond' => 1,
            'millisecond' => 1000,
            'second' => 1e+6,
            'minute' => 6e+7,
            'hour' => 3.6e+9,
            'day' => 8.64e+10,
            'week' => 6.048e+11,
            'month' => 2.628e+12,
            'year' => 3.154e+13,
            'decade' => 3.154e+14,
            'century' => 3.154e+15,
        ),
        'millisecond' => array(
            'nanosecond' => 1e+6,
            'microsecond' => 1000,
            'millisecond' => 1,
            'second' => 1000,
            'minute' => 60000,
            'hour' => 3.6e+6,
            'day' => 8.64e+7,
            'week' => 6.048e+8,
            'month' => 2.628e+9,
            'year' => 3.154e+10,
            'decade' => 3.154e+11,
            'century' => 3.154e+12,
        ),
        'second' => array(
            'nanosecond' => 1e+9,
            'microsecond' => 1e+6,
            'millisecond' => 1000,
            'second' => 1,
            'minute' => 60,
            'hour' => 3600,
            'day' => 86400,
            'week' => 604800,
            'month' => 2.628e+6,
            'year' => 3.154e+7,
            'decade' => 3.154e+8,
            'century' => 3.154e+9,
        ),
        'minute' => array(
            'nanosecond' => 6e+10,
            'microsecond' => 6e+7,
            'millisecond' => 60000,
            'second' => 60,
            'minute' => 1,
            'hour' => 60,
            'day' => 1440,
            'week' => 10080,
            'month' => 43800.048,
            'year' => 525600,
            'decade' => 5.256e+6,
            'century' => 5.256e+7,
        ),
        'hour' => array(
            'nanosecond' => 3.6e+12,
            'microsecond' => 3.6e+9,
            'millisecond' => 3.6e+6,
            'second' => 3600,
            'minute' => 60,
            'hour' => 1,
            'day' => 24,
            'week' => 168,
            'month' => 730.001,
            'year' => 8760,
            'decade' => 87600,
            'century' => 876000,
        ),
        'day' => array(
            'nanosecond' => 6.048e+14,
            'microsecond' => 8.64e+10,
            'millisecond' => 8.64e+7,
            'second' => 86400,
            'minute' => 1440,
            'hour' => 24,
            'day' => 1,
            'week' => 7,
            'month' => 30.417,
            'year' => 365,
            'decade' => 3650,
            'century' => 36500,
        ),
        'week' => array(
            'nanosecond' => 6.048e+14,
            'microsecond' => 6.048e+11,
            'millisecond' => 6.048e+8,
            'second' => 604800,
            'minute' => 10080,
            'hour' => 168,
            'day' => 7,
            'week' => 1,
            'month' => 4.345,
            'year' => 52.143,
            'decade' => 521.429,
            'century' => 5214.286,
        ),
        'month' => array(
            'nanosecond' => 2.628e+15,
            'microsecond' => 2.628e+12,
            'millisecond' => 2.628e+9,
            'second' => 2.628e+6,
            'minute' => 43800.048,
            'hour' => 730.001,
            'day' => 30.417,
            'week' => 4.345,
            'month' => 1,
            'year' => 12,
            'decade' => 120,
            'century' => 1199.999,
        ),
        'year' => array(
            'nanosecond' => 3.154e+16,
            'microsecond' => 3.154e+13,
            'millisecond' => 3.154e+10,
            'second' => 3.154e+7,
            'minute' => 525600,
            'hour' => 8760,
            'day' => 365,
            'week' => 52.143,
            'month' => 12,
            'year' => 1,
            'decade' => 10,
            'century' => 100,
        ),
        'decade' => array(
            'nanosecond' => 3.154e+17,
            'microsecond' => 3.154e+14,
            'millisecond' => 3.154e+11,
            'second' => 3.154e+8,
            'minute' => 5.256e+6,
            'hour' => 87600,
            'day' => 3650,
            'week' => 521.429,
            'month' => 120,
            'year' => 10,
            'decade' => 1,
            'century' => 10,
        ),
        'century' => array(
            'nanosecond' => 3.154e+18,
            'microsecond' => 3.154e+15,
            'millisecond' => 3.154e+12,
            'second' => 3.154e+9,
            'minute' => 5.256e+7,
            'hour' => 876000,
            'day' => 36500,
            'week' => 5214.286,
            'month' => 1199.999,
            'year' => 100,
            'decade' => 10,
            'century' => 1,
        ),
    );
}