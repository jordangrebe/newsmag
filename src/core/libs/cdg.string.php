<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
namespace Coddyger;

class cdg_string
{
// Truncate String
    public static function truncate_text($text,$new_length){
        $new_length = abs((int)$new_length);
        if(strlen($text) > $new_length) {
            $text = preg_replace("/^(.{1,$new_length})(\\s.*|$)/s", '\\1...', $text);
        }
        return($text);
    }
    // Highlight word or sentence in String
    public static function highlight_word($text, $words) {
        $highlighted = preg_filter('/' . preg_quote($words) . '/i', '<span class="mainColor">$0</span>', $text);
        if (!empty($highlighted)) {
            $text = $highlighted;
        }
        return $text;
    }
    public static function highlight($text, $words, $color) {
        $highlighted = preg_filter('/' . preg_quote($words) . '/i', '<span style="color: ' . $color . '">$0</span>', $text);
        if (!empty($highlighted)) {
            $text = $highlighted;
        }
        return $text;
    }
    // Remove special characters from string
    public static function remove_special_char($string) {
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
    // Remove string from another string
    public static function remove_from_string($remove, $from){
        if(!empty($from)){
            return str_replace($remove, '', $from);
        }else{
            return 0;
        }
    }
    public static function do_link($string, $label, $target = ''){
        if(!empty($string)){
            return '<a href="' . BASEURL . $string . '" target="' . $target . '">' . $label . '</a>';
        }else {
            return 'no_link_error';
        }
    }
    public static function name_format($string) {
        return ucwords(strtolower($string));
    }
    /*
     * Return null if var is empty or null
     */
    public static function null_case($var){
        return (empty($var) || is_null($var) ? null : $var);
    }
    public static function is_empty($var){
        return (empty($var) || is_null($var) ? true : false);
    }
    // Give
    public static function buildAcronym($string, $length = 1) {
        $words = explode(" ", $string);
        $acronym = "";
        $length = (self::is_empty($string) || $length <= 0 ? 1 : $length);

        foreach ($words as $i => $w) {
            $i += 1;
            if($i <= $length) {
                $acronym .= $w[0];
            }
        }

        return $acronym;
    }
}