<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
namespace Coddyger;

class cdg_activity
{
    private static $_default_type = 'alert';

    public static function do_activity($text, $type = null, $object = null)
    {
        $db = cdg_database::connect();
        $db->query("set names utf8");

        // --- Set activity type to default on empty or null
        $type = !empty($type) ? $type : self::$_default_type;

        // ============
        $ip = Coddyger::init_get_ip();
        $browser = Coddyger::init_browser();
        // Query Parameter
        $S = $db->prepare('INSERT INTO cdg_activity (activity_type, activity_text, activity_user, activity_ip, activity_browser) VALUES(:d_type,:d_text,:d_user,:d_ip,:d_browser)');
        $S->bindValue(':d_type',$type,\PDO::PARAM_STR);
        $S->bindValue(':d_text',$text,\PDO::PARAM_STR);
        $S->bindValue(':d_user',$object,\PDO::PARAM_STR);
        $S->bindValue(':d_ip',$ip,\PDO::PARAM_STR);
        $S->bindValue(':d_browser',$browser,\PDO::PARAM_STR);
        $S->execute();
        if($S){
            return true;
        } else {
            return false;
        }
    }
    public static function do_event($desc, $object, $user)
    {
        $db = cdg_database::connect();
        $db->query("set names utf8");
        // ============
        $ip = Coddyger::init_get_ip();
        $browser = Coddyger::init_browser();
        $events_env = $ip . ', ' . $browser;
        // Query Parameter
        $S = $db->prepare('INSERT INTO app_events (events_object, events_users, events_desc, events_env) VALUES(:events_object, :events_users, :events_desc, :events_env)');
        $S->bindValue(':events_object',$object,\PDO::PARAM_STR);
        $S->bindValue(':events_users',$user,\PDO::PARAM_STR);
        $S->bindValue(':events_desc',$desc,\PDO::PARAM_STR);
        $S->bindValue(':events_env',$events_env,\PDO::PARAM_STR);
        $S->execute();
    }
    public static function do_notif_seen($notif_id){
        $check_notif_exist = cdg_database::check_existence('cdg_activity', 'activity_id', $notif_id);
        if($check_notif_exist){
            $db = cdg_database::connect();
            $Q = $db->prepare('UPDATE cdg_activity SET activity_status = :item_status WHERE activity_id = :notif_id');
            $Q->bindValue(':notif_id', $notif_id, \PDO::PARAM_STR);
            $Q->bindValue(':item_status', 'seen', \PDO::PARAM_STR);
            $Q->execute();

            if($Q){
                return true;
            }else{
                return false;
            }
        }else{
            return 'not_found';
        }
    }

    private static function activity_by_user($user) {
        $db = cdg_database::connect();
        $db->query("set names utf8");

        $Q = $db->prepare('SELECT * FROM cdg_activity WHERE activity_user = :data_user ORDER BY activity_reg DESC');
        $Q->bindValue(':data_user', $user, \PDO::PARAM_STR);
        $Q->execute();

        $count_data = $Q->rowCount();

        if($count_data >= 1){
            return $Q->fetch();
        }else{
            return false;
        }
    }
    private static function activity_by_user_type($type, $user) {
        $db = cdg_database::connect();
        $db->query("set names utf8");

        $Q = $db->prepare('SELECT * FROM cdg_activity WHERE activity_user = :data_user AND activity_type = :data_type ORDER BY activity_reg DESC');
        $Q->bindValue(':data_user', $user, \PDO::PARAM_STR);
        $Q->bindValue(':data_type', $type, \PDO::PARAM_STR);
        $Q->execute();

        $count_data = $Q->rowCount();

        if($count_data >= 1){
            return $Q->fetch();
        }else{
            return false;
        }
    }
    private static function activity_by_type($type) {
        $db = cdg_database::connect();
        $db->query("set names utf8");

        $Q = $db->prepare('SELECT * FROM cdg_activity WHERE activity_type = :data_type ORDER BY activity_reg DESC');
        $Q->bindValue(':data_type', $type, \PDO::PARAM_STR);
        $Q->execute();

        $count_data = $Q->rowCount();

        if($count_data >= 1){
            return $Q->fetch();
        }else{
            return false;
        }
    }
    public static function _init_activities($slug, $data_format = 'array', $datatable = false)
    {
        $_grant_access_ = cdg_security::_grant_access_(1, $slug);
        if($_grant_access_){
            $db = cdg_database::connect();
            $db->query("set names utf8");
            $array = null;
            // Query Parameter
            $Q = $db->prepare('SELECT * FROM cdg_activity ORDER BY activity_reg DESC');
            $Q->bindValue(':customers_status', 'active', \PDO::PARAM_STR);
            $Q->execute();

            $count_array_data = $Q->rowCount();

            while ($data = $Q->fetch()){
                $array[] = $data;
            }

            if($data_format == 'json'){
                if($count_array_data >= 1) {
                    $json = null;
                    $i = 0;
                    $x = null;
                    $all = null;

                    foreach($array as $j => $tmp) {
                        $data_id = Coddyger::sanitize_output($tmp['activity_id']);
                        $data_text = Coddyger::sanitize_output($tmp['activity_text']);
                        $data_status = Coddyger::sanitize_output($tmp['activity_status']);
                        $data_reg = Coddyger::sanitize_output($tmp['activity_reg']);
                        /*
                        |------------------
                        | JSON OUTPUT ---------
                        |------------------
                        */
                        $x['data_id'] = $data_id;
                        $x['data_text'] = $data_text;
                        $x['data_status'] = $data_status;
                        $x['data_reg'] = $data_reg;

                        $all[$i] = $x;
                        $i++;
                    }
                    $json['json_count'] = (int)$count_array_data;
                    ($datatable ? $json = $all : $json['json_data'] = $all);
                }else{
                    if($datatable){
                        $json = 'no_data_found';
                    }else{
                        $json['json_count'] = 0;
                        $json['json_data'] = 0;
                    }
                }
                header('Content-Type: application/json');
                return json_encode($json);
            }
            else{
                return $array;
            }
        }else{
            return 'access_denied';
        }
    }
}