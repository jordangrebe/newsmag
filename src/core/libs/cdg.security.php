<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
namespace Coddyger;

class cdg_security
{
    public static function do_security($user, $type, $code, $token) {
        $db = cdg_database::connect();
        $db->query("set names utf8");
        // -----------------------------------
        $Q = $db->prepare('INSERT INTO cdg_security (code, token, type, expire, user) VALUES (:a,:b,:c, DATE_ADD(DATE(NOW()), INTERVAL 1 DAY), :d)');
        
        $Q->bindValue(':a',$code,\PDO::PARAM_STR);
        $Q->bindValue(':b',$token,\PDO::PARAM_STR);
        $Q->bindValue(':c',$type,\PDO::PARAM_STR);
        $Q->bindValue(':d',$user,\PDO::PARAM_STR);
        $Q->execute();
        if($Q) {
            return true;
        } else{
            return false;
        }
    }
    public static function update_security($code) {
        $db = cdg_database::connect();
        $db->query("set names utf8");
        // -----------------------------------
        $Q = $db->prepare('UPDATE cdg_security SET status = 0, updated_at = NOW() WHERE code = :x');
        $Q->bindValue(':x',$code,\PDO::PARAM_INT);
        $Q->execute();
        if($Q){
            return 'success';
        }else{
            return 'error';
        }
    }
    public static function remove_security($user_ref, $security_code) {
        $db = cdg_database::connect();
        $db->query("set names utf8");
        // -----------------------------------
        $Q = $db->prepare('DELETE FROM cdg_security WHERE code = :a AND user = :b');
        $Q->bindValue(':a',$security_code,\PDO::PARAM_INT);
        $Q->bindValue(':b',$user_ref,\PDO::PARAM_STR);
        $Q->execute();
        if($Q){
            return true;
        }else{
            return false;
        }
    }
    public static function check_security_status($slug, $code){
        $db = cdg_database::connect();
        // ----------------------------=
        $Q = $db->prepare('SELECT * FROM cdg_security WHERE (user = :x AND code = :y) AND (status = 1)');
        $Q->bindValue(':x', $slug, \PDO::PARAM_STR);
        $Q->bindValue(':y', $code, \PDO::PARAM_INT);
        $Q->execute();
        if($Q->rowCount() >= 1) {
            return true;
        } else {
            return false;
        }
    }
    public static function _grant_access_($required_level, $user_ref){
        $data_current_user = cdg_database::fetch_table_data('app_users', 'slug', $user_ref);
        if($data_current_user != 'no_data'){
            $current_user_level = (int)$data_current_user['grade'];
            $required_level = (int)$required_level;

            if($current_user_level >= $required_level){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public static function _check_token_status_($token){
        $db = cdg_database::connect();
        // ----------------------------=
        $Q = $db->prepare('SELECT status FROM cdg_security WHERE token = :x');
        $Q->bindValue(':x', $token, \PDO::PARAM_STR);
        $Q->execute();
        $data = $Q->fetch();
        if($Q->rowCount() >= 1){
            return (int)$data['security_status'];
        }else{
            return (int)$data['security_status'];
        }
    }
    public static function create_id($length = 45) {
        try{
            $bytes = 0;
            if (function_exists("random_bytes")) {
                $bytes = random_bytes(ceil($length / 2));
            } elseif (function_exists("openssl_random_pseudo_bytes")) {
                $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
            } else {
                print ("no cryptographically secure random function available");
            }
            return substr(bin2hex($bytes), 0, $length);
        }catch(\Exception $e){
            Coddyger::init_system_alert('danger',$e->getMessage(),'');
        }
        return 0;
    }
    public static function create_password($length, $add_dashes, $available_sets)
    {
        $sets = array();
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if(!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($password) > $dash_len)
        {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }
    public static function password_strength($password) {
        if (strlen($password) < 8) {
            return 'too_short';
        }elseif (!preg_match("#[0-9]+#", $password)) {
            return 'no_number';
        }elseif (!preg_match("#[a-zA-Z]+#", $password)) {
            return 'no_letter';
        }else{
            return 0;
        }
    }
    public static function format_reference($ref){
        return (empty($ref) || is_null($ref) ? 'error' : str_replace('cdg.', '', $ref));
    }
    public static function access_granted($dir, $user){
        if(self::_module_access_($dir, $user) !== true){
            header('location:' . BASEURL . 'e001?mod=' . $dir);
        }
    }
    private static function _module_access_($mod_ref, $user_ref){
        if(!self::_is_empty_($mod_ref) || !self::_is_empty_($user_ref)){
            $db = cdg_database::db();
            $Q = $db->prepare('SELECT * FROM app_module_user WHERE module_user_module = :obj1 AND module_user_user = :obj2');
            $Q->bindValue(':obj1', $mod_ref, \PDO::PARAM_STR);
            $Q->bindValue(':obj2', $user_ref, \PDO::PARAM_STR);
            $Q->execute();

            if($Q){
                if($Q->rowCount() >= 1){
                    return true;
                }else{
                    cdg_activity::do_activity('Matricule :: ' . $user_ref . ' a tenté un accès au module user', 'alert');
                    return false;
                }
            }else{
                return 'mysql_query';
            }
        }else{
            return 'empty_data';
        }
    }
    private static function _is_empty_($var){
        return (empty($var) || is_null($var) || strlen($var) <= 0 ? true : false);
    }
    public static function compare_password ($needle, $native) {
        return (password_verify($needle, $native) ? true : false);
    }
    public static function buildRef($len = 13, $acronym = '') {
        $prefix = rand(100, 900);
        $content = cdg_security::create_password($len, false, 'ud');
        $suffix = rand(900, 1500);

        $acronym = (cdg_string::is_empty($acronym) ? $acronym : '[' . $acronym . ']');

        return $acronym . $prefix . '-' . $content . '-' . $suffix;
    }
}
