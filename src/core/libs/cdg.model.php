<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

class Model
{
    private ?string $table = null;
    
    function __construct(string $table)
    {
        $this->table = $table;
    }

    /* ------------------------------------
    | SELECTIONNE DES DOCUMENTS
     ----------------------------------- */
    function select()
     {
         try {
             $Q = MysqlSet::run('SELECT * FROM ' . $this->table . ' ORDER BY created_at DESC');
 
             return $Q->fetchAll();
         } catch (Exception $e) {
             return ['error' => true, 'data' => $e];
         }
     }

    /* ------------------------------------
    | SELECTIONNE DETAILS D'UN DOCUMENT
     ----------------------------------- */
    function selectOne(string $slug)
    {
        
        try {
            $Q = MysqlSet::run('SELECT * FROM ' . $this->table . ' 
             WHERE slug = ?', [$slug]);

            if (!$Q) {
                return false;
            } else {
                return $Q->fetch();
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }
    /* ------------------------------------
    | SUPPRIME UN DOCUMENT
     ----------------------------------- */
    function remove(string $slug)
    {
        try {
            $Q = MysqlSet::run('DELETE FROM ' . $this->table . ' WHERE slug = ?', [$slug]);

            if ($Q) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    function ownData(string $slug, object $payloads)
    {
        try {
            $Q = MysqlSet::run('SELECT * FROM ' . $this->table . '  WHERE slug = ? AND ' . $payloads->column . ' = ?', [$slug, $payloads->data]);

            if (!$Q) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }
}
