<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
namespace Coddyger;

class cdg_database
{
    public static function create()
    {
        // Coming Soon
        return 'Instructions to create new database.';
    }
    public static function connect()
    {
        try{
            $connexion = new \PDO('mysql:host='.DBHOST.';dbname='.DBNAME.'', DBUSER, DBPASS, array(
                \PDO::ERRMODE_EXCEPTION
            ));

            return $connexion;
        }catch(\Exception $e){
            die('Failed to connect to Database :: '.$e->getMessage());
        }
    }
    public static function uSQLdelete(array $items)
    {
        if(!array_key_exists('table',$items)){
            die('No table given.');
        }elseif(!array_key_exists('column_key',$items)){
            die('No index given.');
        }elseif(!array_key_exists('data_value',$items)){
            die('No value given.');
        }else{
            if($items['multiple'] == 'yes'){
                $Q = 'DELETE FROM '.$items['table'].' ';
            }
            else{
                $Q = "DELETE FROM ".$items['table']." WHERE ".$items['column_key']." = ".$items['data_value'];
            }
        }
        return $Q;
    }
    public static function uSQLselect(array $entity)
    {
        if(array_key_exists('table', $entity)){
            if(array_key_exists('type_selec', $entity)){
                $query = $entity['type_selec']." ";
            }
            else{
                $query = "SELECT ";
            }
            if(array_key_exists('params', $entity)){
                foreach($entity['params'] as $prop){
                    if(end($entity['params']) == $prop){

                        $query .= $prop." FROM ";
                    }else{
                        $query .= $prop.", ";
                    }
                }
            }
            else{
                die('No parameter given.');
            }
            $query .= $entity['table']. " ";
            if(array_key_exists('clause_whre', $entity)){
                if($entity['clause_whre']){
                    if(array_key_exists('args_whre', $entity)){
                        foreach($entity['args_whre'] as $condition){
                            if(!array_key_exists('prop',$condition)){
                                die('No property given');
                            }elseif(!array_key_exists('value',$condition)){
                                die('No value given');
                            }else{
                                if(array_key_exists('set_or',$condition)){
                                    if($condition['set_or']){
                                        if(end($entity['args_whre'])['prop'] == $condition['prop'] && $entity['args_whre'][0]['prop'] != $condition['prop']){
                                            $query .= "OR ".$condition['prop']." = '".$condition['value']."'";
                                        }else if($entity['args_whre'][0]['prop'] == $condition['prop'] && end($entity['args_whre'])['prop'] != $condition['prop']){
                                            $query .= "WHERE ".$condition['prop']." = '".$condition['value']."' ";
                                        }
                                        else if($entity['args_whre'][0]['prop'] == $condition['prop'] && end($entity['args_whre'])['prop'] == $condition['prop']){
                                            $query .= "WHERE ".$condition['prop']." = '".$condition['value']."'";
                                        }
                                        else{
                                            $query .= "OR ".$condition['prop']." = '".$condition['value']."' ";
                                        }
                                    }
                                    else{
                                        if(end($entity['args_whre'])['prop'] == $condition['prop'] && $entity['args_whre'][0]['prop'] != $condition['prop']){
                                            $query .= "AND ".$condition['prop']." = '".$condition['value']."'";
                                        }else if($entity['args_whre'][0]['prop'] == $condition['prop'] && end($entity['args_whre'])['prop'] != $condition['prop']){
                                            $query .= "WHERE ".$condition['prop']." = '".$condition['value']."' ";
                                        }
                                        else if($entity['args_whre'][0]['prop'] == $condition['prop'] && end($entity['args_whre'])['prop'] == $condition['prop']){
                                            $query .= "WHERE ".$condition['prop']." = '".$condition['value']."'";
                                        }
                                        else{
                                            $query .= "AND ".$condition['prop']." = '".$condition['value']."' ";
                                        }
                                    }
                                }
                                else{
                                    if(end($entity['args_whre'])['prop'] == $condition['prop'] && $entity['args_whre'][0]['prop'] != $condition['prop']){
                                        $query .= "AND ".$condition['prop']." = '".$condition['value']."'";
                                    }else if($entity['args_whre'][0]['prop'] == $condition['prop'] && end($entity['args_whre'])['prop'] != $condition['prop']){
                                        $query .= "WHERE ".$condition['prop']." = '".$condition['value']."' ";
                                    }
                                    else if($entity['args_whre'][0]['prop'] == $condition['prop'] && end($entity['args_whre'])['prop'] == $condition['prop']){
                                        $query .= "WHERE ".$condition['prop']." = '".$condition['value']."'";
                                    }
                                    else{
                                        $query .= "AND ".$condition['prop']." = '".$condition['value']."' ";
                                    }
                                }
                            }
                        }

                    }
                    else{
                        die('No argument given.');
                    }
                }
            }
            if(array_key_exists('orderby', $entity)){
                $query .= " ORDER BY ";
                foreach($entity['orderby'] as $prop){
                    if(end($entity['orderby']) == $prop){
                        $query .= $prop." ";
                    }else{
                        $query .= $prop.", ";
                    }
                }
            }
            if(array_key_exists('lim', $entity)){
                $query .= " LIMIT ". $entity['lim']['start'].",". $entity['lim']['end']."";
            }
            return $query;
        }
        else{
            die('No table given.');
        }
    }
    public static function check_existence($table, $column, $key){
        $db = cdg_database::connect();
        $db->query("set names utf8");

        // Query Parameter
        $Q = $db->prepare('SELECT * FROM ' . $table . ' WHERE ' . $column . ' = :key');
        $Q->bindValue(':key',$key , \PDO::PARAM_STR);
        $Q->execute();

        if($Q->rowCount() >= 1){
            return true;
        }else{
            return false;
        }
    }
    public static function fetch_table_data($table, $column = '', $key = ''){
        if(empty($table)){
            return 'empty_table';
        }else{
            if(!empty($key) && !empty($column)){
                $Q = self::db()->prepare('SELECT * FROM ' . $table . ' WHERE ' . $column . ' = :get_key');
                $Q->bindValue(':get_key', $key, \PDO::PARAM_STR);
            }else{
                $Q = self::db()->prepare('SELECT * FROM ' . $table);
            }
            $Q->execute();
            if($Q->rowCount() >= 1){
                return $Q->fetch();
            }else{
                return 'no_data';
            }
        }
    }
    public static function dates_list($new_date, $old_date){
        // Can generate only 100000 dates between 2 periodes
        $Q = self::connect()->prepare("SELECT ADDDATE('" . $old_date . "', INTERVAL @i:=@i+1 DAY) AS DATELIST
FROM (
SELECT a.a
FROM (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS a
CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS b
CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS c
CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS d
CROSS JOIN (SELECT 0 AS a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) AS e
) a
JOIN (SELECT @i := -1) r1
WHERE 
@i < DATEDIFF('" . $new_date . "', '" . $old_date . "')");
        $Q->execute();

        return $Q->fetchAll();
    }
    public static function months_from_dates($new_date, $old_date){
        $db = self::connect();
        $Q = $db->prepare("select m1 from ( select ('" . $old_date . "' - INTERVAL DAYOFMONTH('" . $old_date . "')-1 DAY) + INTERVAL m MONTH as m1 from 
(select @rownum:=@rownum+1 as m from
(select 1 union select 2 union select 3 union select 4) t1,
(select 1 union select 2 union select 3 union select 4) t2,
(select 1 union select 2 union select 3 union select 4) t3,
(select 1 union select 2 union select 3 union select 4) t4,
(select @rownum:=-1) t0) d1) d2 where m1 <= '" . $new_date . "' order by m1");
        $Q->execute();

        return $Q->fetchAll();
    }
    public static function db(){
        $db = self::connect();
        $db->query("set names utf8");
        return $db;
    }
}