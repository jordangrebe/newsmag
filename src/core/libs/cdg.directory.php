<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
namespace Coddyger;

class cdg_directory
{
    const WritePermission = 0777;
    const WriteMode = array('r', 'r+', 'w', 'w+', 'a', 'a+', 'x', 'x+', 'c', 'c+', 'e');
    // New Folder ---------------------
    public static function create_dir($dir_name, $permission)
    {
        if(empty($permission)){
            $permission = self::WritePermission;
        }
        if(empty($dir_name)){
            Coddyger::init_system_alert('warning','Directory name is empty.','Failed to create directory.');
        }elseif(preg_match("#/[^a-z0-9 // ]+/i#", $dir_name)){
            Coddyger::init_system_alert('warning','Directory name cannot contains special character.','Failed to create directory.');
        }else{
            if(is_dir($dir_name)){
                Coddyger::init_system_alert('warning','Directory with such name already exist.','Failed to create directory.');
            }else{
                mkdir($dir_name, $permission, true);
                if(is_dir($dir_name)){
                    return 'ok';
                }else{
                    Coddyger::init_system_alert('warning','Unknown error.','Failed to create directory in : ' . $dir_name);
                    return 'error';
                }
            }
        }
        return 0;
    }
    // Delete Folder -----------------
    public static function delete_dir($dir_path)
    {
        if($dir_path == ''){
            Coddyger::init_system_alert('warning','Directory name is empty.','Failed to delete directory in : ' . $dir_path);
        }else{
            if(!is_dir($dir_path)){
                Coddyger::init_system_alert('warning','Directory with such name do not exist.','Failed to delete directory in : ' . $dir_path);
            }else{
                if(is_dir($dir_path)){
                    array_map('unlink',glob($dir_path . "/*.*"));
                    if(rmdir($dir_path) == TRUE){
                        return true;
                    }else{
                        Coddyger::init_system_alert('warning','Unable to delete such directory.','Failed to delete directory in : ' . $dir_path);
                    }
                }else{
                    Coddyger::init_system_alert('warning','Directory with such name do not exist.','Failed to delete directory in : '.$dir_path);
                }
            }
        }
        return false;
    }
    // Count Directories
    public static function count_dir($path){
        $dirs = array();
        $dir = scandir($path);

        foreach($dir as $files){
            if(is_dir("$path")){
                $dirs[] = $files;
            }
        }
        return count($dirs) - 2;
    }
    public static function count_file($path){
        $version = explode('.', PHP_VERSION);
        if ($version[0] > 5 || ($version[0] == 5 && $version[1] >= 4)) {
            $fi = new \FilesystemIterator($path, \FilesystemIterator::SKIP_DOTS);
            return iterator_count($fi);
        } else {
            return count(glob($path . DS . '*'));
        }
    }
    // Find and display files -----
    public static function check_file($file_path, $file_name)
    {
        if(is_file($file_path . DS . $file_name)){
            require_once $file_path . DS . $file_name . '';
        }else{
            die('Requested file do not exist in ' . $file_path . '. Unable to load :: ' . $file_name);
        }
    }
    // List Directory ------
    public static function get_dir($path)
    {
        $dirs = array();
        if ($handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {

                if ($entry != "." && $entry != "..") {

                    $dirs[] = $entry;
                }
            }
            closedir($handle);
        }
        return $dirs;
    }
    // List Package ----------
    public static function package_list($package_dir)
    {
        if ($handle = opendir($package_dir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && preg_match('#^module#',$entry)) {
                    $dirs[] = $entry;
                }
            }
            closedir($handle);
        }
        if(empty($dirs)){
            $dirs = ('no_data');
            return $dirs;
        }else{
            return $dirs;
        }
    }
    public static function copy_file($from_path, $to_path, $filename, $delete_from = false){
        if(copy($from_path . DS . $filename, $to_path . DS . $filename)){
            if($delete_from){
                self::delete_dir($from_path);
            }
            return true;
        }else{
            return false;
        }
    }
    public static function file_size_convert($bytes)
    {
        $result = null;
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }
    // New file
    public static function create_file($filename, $mode, $content = null){
        $alert = null;
        if(!empty($filename) && !empty($mode)){
            if(in_array($mode, self::WriteMode)){
                $create_file = fopen($filename,$mode);
                if(!is_null($content) || !empty($content)){
                    fputs($create_file, $content);
                }
                fclose($create_file);
            }else{
                $alert = 'Write mode not found';
                $create_file = false;
            }
        }else{
            $alert = 'filename and mode sould not be empty';
            $create_file = false;
        }

        return ($create_file === false ? $alert : 0);
    }
}