<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
namespace Coddyger;

class cdg_curl
{
    public static function post(array $data) {
        if(cdg_string::is_empty($data)){
            return 'is_empty';
        } else {
            $ch = curl_init();

            $url = $data['url'];
            $items = $data['items'];

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($items));
            // Receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec($ch);

            curl_close ($ch);
            // Further processing ...
            return $server_output;
        }
    }
    public static function get(array $data) {

    }
}