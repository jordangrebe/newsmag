<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
namespace Coddyger;

class cdg_upload
{
    private static $default_size = 100 * 1024; // 100 Kilo Octets
    private static $default_type = array('png','jpeg','exe','mp3','mp4');
    private static $octet = 1024;

    public static function check_size($user_default_size,$file_size){
        $user_default_size = $user_default_size * self::$octet;

        if($user_default_size >= 1 || !empty($user_default_size)){
            self::$default_size = $user_default_size;
        }

        if($file_size > self::$default_size){
            return 'file_too_large';
        }else{
            return 'size_ok';
        }
    }
    public static function check_type($user_default_type,$file_type){
        $file_type = strtolower(pathinfo($file_type,PATHINFO_EXTENSION));
        if(empty($user_default_type)){
            $user_default_type = self::$default_type;
        }
        self::$default_type = (array)$user_default_type;

        if(in_array($file_type,self::$default_type,TRUE)){
            return 'type_allowed';
        }else{
            return 'type_not_allowed';
        }
    }
    public static function generate_filename($filename, $original_filename = false){
        $file_extension = strtolower(pathinfo($filename,PATHINFO_EXTENSION));
        if($original_filename == true){
            $filename = cdg_security::create_id(20).'-'.$filename;
        }else{
            $filename = cdg_security::create_id(30).'.'.$file_extension;
        }

        return strtolower($filename);
    }
    public static function SaveFile($file_tmp_name,$path){
        if (move_uploaded_file($file_tmp_name, $path)) {
            return 'file_uploaded';
        } else {
            return 'file_not_upload';
        }
    }
}