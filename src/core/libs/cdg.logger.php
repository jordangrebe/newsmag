<?php
namespace Coddyger;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class cdg_logger
{
    public static function log($content, $module = 'default', $type = 'error')
    {
        try {
            // create a log channel
            $log = new Logger('Coddyger');
            $log->pushHandler(new StreamHandler(ROOT_PATH . '/logs/' . $module . '.log', Logger::WARNING));

            if($type === 'warning') {
                $log->warning(json_encode($content));
            } else {
                $log->error(json_encode($content));
            }
        } catch (\PDOException $e) {
            print "Erreur !: " . $e->getMessage() . "<br/>";
            return ['error' => true, 'payload' => ''];
        }
    }
}
