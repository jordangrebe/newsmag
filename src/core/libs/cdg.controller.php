<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

use Coddyger\cdg_logger;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\Extra\Intl\IntlExtension;
use Twig\Extra\String\StringExtension;

class Controller {
    static function buildErrorPayload(string $var)
    {
        return (object) array('payload' => $var);
    }
    public function buildView(string $uri, string $dir, array $data, string $file)
    {
        try {
            $viewpath = ROOT_PATH . VIEWS . DS . $dir;
            $file = empty($file) ? $uri . '.twig' : $file;
            $filepath = $viewpath . DS . $file;

            $loader = new FilesystemLoader($viewpath);
            $twig = new Environment($loader);
            $twig->addExtension(new IntlExtension());
            $twig->addExtension(new StringExtension());

            ob_start();

            if (!file_exists($filepath)) {
                cdg_logger::log(content: 'Unable to find such file :: "' . $file . '" into ' . $viewpath, module: __CLASS__);
                header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
                die('Undefined error occurred. Come back later.');
            } else {
                // require_once $packages_directory . DS . $package_dir . DS . $package_view . '';
                print $twig->render($file, $data['page_data']);
            }
            $package_content = ob_get_clean();
            if (file_exists(ROOT_PATH . PUBLICS . DS . 'index.html')) {
                require_once ROOT_PATH . PUBLICS . DS . 'index.html';
            } else {
                cdg_logger::log(content: 'view controller not found', module: __CLASS__);
                require_once ROOT_PATH . PUBLICS . DS . 'no_view_controller.html';
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }
}