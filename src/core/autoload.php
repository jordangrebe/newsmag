<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
class autoload
{
    public function __construct()
    {
        spl_autoload_register('autoload::Autoloader');
    }
    private static $class_path;
    private static $class_file;

    protected static function AutoLoader($class) {
        try {
            if (preg_match('#Set#', $class)) {
                self::$class_file = self::buildFile('set', $class);
                self::$class_path = ROOT_PATH . MODELS . DS . 'sets' . DS . self::$class_file;
            } else if (preg_match('#Model#', $class)) {
                self::$class_file = self::buildFile('model', $class);
                self::$class_path = ROOT_PATH . MODELS . DS . self::$class_file;
            } else if (preg_match('#Controller#', $class)) {
                self::$class_file = self::buildFile('controller', $class);
                self::$class_path = ROOT_PATH . CONTROLLERS . DS . self::$class_file;
            } else if (preg_match('#Helper#', $class)) {
                self::$class_file = self::buildFile('helper', $class);
                self::$class_path = ROOT_PATH . HELPERS . DS . self::$class_file;
            } else if (preg_match('#Middleware#', $class)) {
                self::$class_file = self::buildFile('middleware', $class);
                self::$class_path = ROOT_PATH . MIDDLEWARES . DS . self::$class_file;
            } else if (preg_match('#Api#', $class)) {
                self::$class_file = self::buildFile('api', $class);
                self::$class_path = ROOT_PATH . ROUTES . DS . self::$class_file;
            } else if (preg_match('#Web#', $class)) {
                self::$class_file = self::buildFile('web', $class);
                self::$class_path = ROOT_PATH . WEB . DS . self::$class_file;
            } else if (preg_match('#Database#', $class)) {
                self::$class_file = self::buildFile('database', $class);
                self::$class_path = ROOT_PATH . DATABASES . DS . self::$class_file;
            } else {
                if (preg_match('#RouteHandler#', $class)) {
                    self::$class_file = 'handler.php';
                    self::$class_path = ROOT_PATH . ROUTES . DS . self::$class_file;
                } 
                // self::$class_file = self::buildFile('database', $class);
                // self::$class_path = ROOT_PATH . SYS_DIR . DS . self::$class_file;
            }
            
            $is_file = file_exists(self::$class_path);
            
            if($is_file) {
                require_once self::$class_path;
            } else {
                throw new Exception(self::$class_path);
            }
        } catch (Exception $e) {
            echo 'Exception : ',  $e->getMessage(), "\n";
        }
    }

    protected static function buildFile($group, $class) {
        return strtolower(str_replace(ucfirst($group), '', $class)) . '.' . $group . '.php';
    }
}
$sys_autoload = new autoload;
