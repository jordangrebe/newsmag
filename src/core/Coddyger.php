<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

namespace Coddyger {

    use Coddyger\cdg_browser;

    class Coddyger
    {
        private static $param = null;
        public static function init_get_address($param)
        {
            self::$param = $param;
            return (isset($_GET[self::$param])) ? $_GET[self::$param] : null;
        }
        public static function init_get_dir()
        {
            return (isset($_GET['dir'])) ? $_GET['dir'] : '';
        }
        public static function init_get_subdir()
        {
            return (isset($_GET['subdir'])) ? $_GET['subdir'] : '';
        }
        public static function init_get_url()
        {
            return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }
        public static function init_get_ip()
        {
            return $_SERVER['REMOTE_ADDR'];
        }
        public static function is_email_address($email_address)
        {
            return (preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\\.[a-z]{2,4}$#", $email_address) ? true : false);
        }
        public static function sanitize_input($string, $filter = '')
        {
            return (!is_array($string) ? stripslashes($string) : $string);
        }
        public static function sanitize_output($string)
        {
            if (!is_array($string) && !empty($string)) {
                $string = stripslashes($string);
                $string = htmlspecialchars($string);
            }
            return $string;
        }
        public static function init_system_alert($type, $cause, $text)
        {
            die('<b>::' . strtolower('coddyger_error') . '::</b> ' . $text . '<br />' . '<div>' . $cause . '</div>');
        }
        public static function delayed_redir($seconds, $url)
        {
            if (!empty($seconds) || !empty($url)) {
                unset($_POST);
                print '<meta http-equiv="refresh" content="' . $seconds . ',' . $url . '" />';
            }
        }
        public static function init_browser()
        {
            $xbrower = new cdg_browser();
            return $xbrower->getName() . ' ' . $xbrower->getVersion() . ' ' . $xbrower->getPlatformVersion();
        }
        public static function init_greeting(array $greetting)
        {
            $current_time = date('H', time());
            if ($current_time >= 00 && $current_time <= 12) {
                return 'Bonjour';
            } elseif ($current_time > 12 && $current_time <= 23) {
                return 'Bonsoir';
            } else {
                return $current_time;
            }
        }
        public static function is_equal($a, $b)
        {
            return ($a == $b || $a === $b ? true : false);
        }


        public static function api($context, $Q)
        {
            if (is_array($Q)) {
                if((array_key_exists('status', $Q) && array_key_exists('message', $Q)) && array_key_exists('data', $Q)) {
                    $json = array(
                        'message' => $Q['message'],
                        'data' => $Q['data']
                    );
    
                    return $context->send($json)->status($Q['status']);
                } else {
                    $json = array(
                        'message' => 'Expected keys data||message||status from Q query are missing',
                        'data' => null
                    );
    
                    return $context->send($json)->status(500);
                }
            } else {
                $json = array(
                    'message' => 'critical error::parameter two needs to be an array, ' . gettype($Q) . ' detected',
                    'data' => null
                );

                return $context->send($json)->status(500);
            }
        }
        public static function renderJSON(array $Q) {
            http_response_code($Q['status']);
            header('Content-Type: application/json');

            $json = array(
                'status' => $Q['status'],
                'message' => $Q['message'],
                'data' => $Q['data']
            );

            echo json_encode($json);
        }

        public static function buildApiResponse($status, $message, $data)
        {
            // -- NOTE ----------
            return ['status' => $status, 'message' => $message, 'data' => $data];
        }
        
        public static function buildUploadPath($path)
        {
            if (!is_dir($path)) {
                if (cdg_directory::create_dir($path, 0777) == 'ok') {
                    return true;
                } else {
                    return 'directory_creation_error';
                }
            } else {
                return 'success';
            }
        }
        public static function searchArrayValueInArray($value, $array, $key)
        {
            if (is_array($array)) {
                return (in_array($value, array_column($array, $key)) ? true : false);
            } else {
                return false;
            }
        }
        public static function calculateDiscount($price, $discount)
        {
            $price = (cdg_string::is_empty($price) ? 0 : $price);
            $discount = (cdg_string::is_empty($discount) ? 0 : $discount);
            return $price - ($price * ($discount / 100));
        }

        public static function init_system_classes()
        {
            $dirs = array();
            if ($handle = opendir(ROOT_PATH . DS . SYS_DIR . DS . 'libs')) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        if (preg_match('#^cdg.#', $entry)) {
                            $dirs[] = $entry;
                        }
                    }
                }
                closedir($handle);
            }

            foreach ($dirs as $system_class) {
                require ROOT_PATH . DS . SYS_DIR . DS . 'libs' . DS . $system_class . '';
            }
        }
        public static function isForeigner(array $payload) {
            foreach ($payload as $key => $value) {
                $slug = $value['slug'];
            }
        }
        public static function titleToUri(string $title) {
            return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($title, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
        }
    }
    Coddyger::init_system_classes();
}
