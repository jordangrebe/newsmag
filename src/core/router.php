<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

use Coddyger\cdg_protocol;

class SysRouter
{
    const routes_directory = ROUTES;

    public function __construct()
    {
        // --- Force https on option in config.php
        (CDG_PROTOCOL == 'https') ? cdg_protocol::init_secure_connection() : false;
        // --- Getting address variables
        $uri = rtrim( dirname($_SERVER["SCRIPT_NAME"]), '/' );
        $uri = '/' . trim( str_replace( $uri, '', $_SERVER['REQUEST_URI'] ), '/' );
        $uri = urldecode( $uri );

        self::route($uri);
    }

    // --- Set default controller as homepage
    protected static function route(string $uri)
    {
        switch (CDG_STATUS) {
            case 'maintaining':
                die('Mainteance mode');
                break;
            case 'comingsoon':
                die('Coming soon');
                break;
            default:
                new RouteHandler(uri: $uri);
                break;
        }
    }
}

new SysRouter;
