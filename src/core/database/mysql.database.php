<?php
    interface MySQL {
        public function connect();
    }

    class MysqlDatabase implements MySQL {
        private string $host;
        private string $user;
        private string $pass;
        private string $db;

        public function __construct(string $host = 'localhost', string $user = 'root', string $pass = '', string $db = 'test'){
            $this->host = $host;
            $this->user = $user;
            $this->pass = $pass;
            $this->db = $db;
        }

        public function connect(){
            try {
                $db = new PDO('mysql:host='.$this->host.';dbname=' . $this->db, $this->user, $this->pass);
                $db->query("set names utf8");
                
                return $db;
            } catch (PDOException $e) {
                print "Erreur !: " . $e->getMessage() . "<br/>";
                return ['error' => true, 'payload' => ''];
            }
        }
    }

?>