<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
use MongoDB\Client;
use MongoDB\Driver\Manager;

class MongoDbDatabase
{
    /* ------------------------------------
    | CONNECTER BASE DE DONNEES
     ----------------------------------- */
    public static function connect()
    {
        try {
            $client = new MongoDB\Client(MONGODB_CLUSTER . '/' . MONGODB_DATABASE, []);

            return $client;
        } catch (Exception $e) {
            echo 'MongoDbDatabase : ',  $e->getMessage(), "\n";
        }
    }
}
