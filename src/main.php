<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Framework
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

class Main {
    public function __construct()
    {
        try{
            define('ROOT_PATH', __DIR__ .'/');
            foreach ($this->dependencies as $i => $x)
            {
                $name = $x['name'];
                $path = $x['path'];
                $active = $x['active'];

                if(empty($path) || !file_exists($path)){
                    die('Missing dependency :: ' . $name);
                } else{
                    if($active === true) {
                        require_once ($path) . '';
                    }
                }
            }
            $dotenv = Dotenv\Dotenv::createImmutable('../');
            $dotenv->load();
        } catch(Exception $e){
            header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
            die('Critical Error Main :: ' . $e->getMessage());
        }
    }
    private $dependencies = array(
        0 => [
            'name' => 'Vendor Autoload',
            'path' => '../vendor/autoload.php',
            'active' => true
        ],
        1 => [
            'name' => 'Coddyger defines',
            'path' => __DIR__ . '/' . 'core/define.php',
            'active' => true
        ],
        2 => [
            'name' => 'Coddyger Autoload',
            'path' => __DIR__ . '/' . 'core/autoload.php',
            'active' => true
        ],
        3 => [
            'name' => 'Coddyger',
            'path' => __DIR__ . '/' . 'core/Coddyger.php',
            'active' => true
        ],
        4 => [
            'name' => 'Coddyger Router',
            'path' => __DIR__ . '/' . 'core/router.php',
            'active' => true
        ],
        
    );
}
$main = new Main();
