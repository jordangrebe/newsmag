<?php
use Coddyger\cdg_directory;

cdg_directory::check_file(ROOT_PATH . CONTROLLERS, 'error.controller.php');
$controller = new ErrorController();
$controller->build404Page('e404');
