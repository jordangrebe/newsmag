<?php

use Coddyger\cdg_security;
use Coddyger\cdg_string;
use Coddyger\cdg_logger;
use Coddyger\Coddyger;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;

class JwtMiddleware
{
    private static $apikey = JWT_SECRET;
    public static function decode()
    {
        $jwt = null;

        $authHeader = 0;
        $token = '';

        if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $authHeader = $_SERVER['HTTP_AUTHORIZATION'];
            $arr = explode(" ", $authHeader);

            $token = $arr[1];
        }

        if ($token != 'null') {
            try {
                $jwt = new \Firebase\JWT\JWT;
                $jwt::$leeway = 86400;
                $decoded = JWT::decode($token, self::$apikey, array('HS512'));

                $data = $decoded->data;

                $userID = $data->slug;

                return [
                    'response' => 'success',
                    'slug' => $userID
                ];
            } catch (ExpiredException $e) {
                return ['response' => 'expired'];
            } catch (Exception $e) {
                cdg_logger::log($e, 'middleware_get');
                return ['response' => 'Action non autorisée'];
            }
        } else {
            return ['response' => 'access_denied'];
        }
    }
    public static function encode($ID, $expire)
    {
        $tokenId    = cdg_security::create_id(32);
        $issuedAt   = time();
        $notBefore  = $issuedAt + 10;
        $serverName = BASEURL;

        $data = [
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
            'iss'  => $serverName,       // Issuer
            'nbf'  => $notBefore,        // Not before
            'exp'  => $notBefore + $expire,           // Expire
            'data' => [                  // Data related to the signer user
                'slug'   => $ID,
            ]
        ];
        // ---------------------
        return JWT::encode(
            $data,
            JWT_SECRET,
            'HS512'
        );
    }
    public static function generateApiAuth()
    {
        $_POST = json_decode(file_get_contents('php://input'), true);
        if (isset($_POST['apikey'])) {
            $apikey = Coddyger::sanitize_input($_POST['apikey']);

            if (cdg_string::is_empty($apikey)) {
                return Coddyger::buildApiResponse(422, 'Clé de vérification requise', $apikey);
            } else {
                if ($apikey == self::$apikey) {
                    $tokenId = cdg_security::create_id(32);
                    $expire = 7400;
                    $jwt = JwtMiddleware::encode($tokenId, $expire);

                    return Coddyger::buildApiResponse(200, 'Token generated', $jwt);
                } else {
                    return Coddyger::buildApiResponse(422, 'Echèc d\'authentification', $apikey);
                }
            }
        } else {
            return Coddyger::buildApiResponse(422, 'Aucune donnée reçu', $_POST);
        }
    }
    public static function checkToken()
    {
        $jwt = JwtMiddleware::decode();
        if ($jwt['response'] == 'success') {
            $expire = 7400;
            $encode = JwtMiddleware::encode($jwt['slug'], $expire);

            return Coddyger::buildApiResponse(200, 'Token generated', $encode);
        } else {
            return Coddyger::buildApiResponse(422, 'Votre session a expiré.', $jwt);
        }
    }
}
