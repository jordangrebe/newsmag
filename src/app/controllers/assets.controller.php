<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

use Coddyger\Coddyger;

class AssetsController extends \Controller
{
    public function selectCountry()
    {
        try {
            $filepath = ROOT_PATH . DATA . DS . 'countries.json';
            $Q = file_get_contents($filepath);

            $json = json_decode($Q);
            
            return Coddyger::buildApiResponse(200, 'ok', $json);
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    public function sendSocket()
    {
        try {
            
            
            return Coddyger::buildApiResponse(200, 'ok', null);
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }
}
