<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
class HomeController extends \Controller
{
    public $model;
    private $table;
    private $user;

    function __construct(string $user)
    {
        $this->model = new PostModel();
        $this->table = $this->model::table;

        $this->user = $user;
    }

    public function buildHomePage(string $uri) {
        $alaune = $this->model->select(category: 'category.alaune', limit: 5);
        $annonce = $this->model->select(category: 'category.annonce', limit: 5);
        $politique = $this->model->select(category: 'category.politique', limit: 5);
        $societe = $this->model->select(category: 'category.societe', limit: 7);
        $economie = $this->model->select(category: 'category.economie', limit: 4);
        $faitsDivers = $this->model->select(category: 'category.faitsdivers', limit: 5);
        $sport = $this->model->select(category: 'category.sport', limit: 10);
        $culture = $this->model->select(category: 'category.culture', limit: 7);
        $inter = $this->model->select(category: 'category.inter', limit: 4);
        $videos = $this->model->select(category: 'category.videos', limit: 5);
        
        $title = 'Accueil';
        $type = 'website';
        $image = BASEURL . DATA . DS . 'og_image.jpg';;
        $description = "";
        $url = BASEURL . DS . $uri;

        $page_data = array();

        // --- Page methods
        $page_data = array(
            'alaune' => $alaune,
            'annonce' => $annonce,
            'politique' => $politique,
            'societe' => $societe,
            'economie' => $economie,
            'faitsDivers' => $faitsDivers,
            'sport' => $sport,
            'culture' => $culture,
            'inter' => $inter,
            'videos' => $videos,
        );
        // --- Page properties
        $browser_title = CDG_APPNAME;

        $data = Array(
            'browser_title' => $browser_title,
            'title' => $title,
            'type' => $type,
            'image' => $image,
            'description' => $description,
            'url' => $url,
            'page_data' => $page_data,
        );

        self::buildView(uri: $uri, dir:'home', data: $data, file: 'index.twig');
    }
    public function buildAboutPage(string $uri) {
        $title = 'About';
        $type = 'website';
        $image = BASEURL . DATA . DS . 'og_image.jpg';;
        $description = "";
        $url = BASEURL . DS . $uri;

        $page_data = array();

        // --- Page methods
        $page_data = array(
            'name' => 'Walter'
        );
        // --- Page properties
        $browser_title = $title . ' - ' . CDG_APPNAME;

        $data = Array(
            'browser_title' => $browser_title,
            'title' => $title,
            'type' => $type,
            'image' => $image,
            'description' => $description,
            'url' => $url,
            'page_data' => $page_data,
        );

        self::buildView(uri: $uri, dir:'home', data: $data, file: '');
    }
}
