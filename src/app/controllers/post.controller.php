<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
class PostController extends \Controller
{
    public $model;
    private $table;
    private $user;

    function __construct(string $user)
    {
        $this->model = new PostModel();
        $this->table = $this->model::table;

        $this->user = $user;
    }

    public function buildPostDetailPage(string $uri, string $slug) {
        $Q = $this->model->selectOne(slug: $slug);
        if($Q) {
            $postDetails = $this->model->buildProps($Q);
            $related = $this->model->select(category: $postDetails['category']['slug'], limit: 7);
            $latest = $this->model->selectLatestRand(limit: 7);

            $title = $postDetails['title'];
            $type = 'website';
            $image = $postDetails['file'];
            $description = "";
            $url = $postDetails['uri'];

            $page_data = array();

            // --- Page methods
            $page_data = array(
                'data' => $postDetails,
                'related' => $related,
                'latest' => $latest,
                'sn' => [SC_FACEBOOK, SC_TWITTER, SC_WHATSAPP, SC_INSTAGRAM],
            );
            // --- Page properties
            $browser_title = $postDetails['title'] . ' - ' . CDG_APPNAME;

            $data = Array(
                'browser_title' => $browser_title,
                'title' => $title,
                'type' => $type,
                'image' => $image,
                'description' => $description,
                'url' => $url,
                'page_data' => $page_data,
            );

            self::buildView(uri: $uri, dir:'post', data: $data, file: 'details.twig');
        } else {
            $ErrorController = new ErrorController('');

            $ErrorController->build404Page(uri: 'e404');
        }
    }
}
