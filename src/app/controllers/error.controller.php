<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

use Coddyger\Coddyger;
use Coddyger\cdg_directory;
use Coddyger\cdg_security;
use Coddyger\cdg_logger;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class ErrorController extends \Controller
{
    public function build404Page(string $uri) {
        $title = 'Page introuvable';
        $type = 'website';
        $image = BASEURL . DATA . DS . 'og_image.jpg';;
        $description = "";
        $url = BASEURL . DS . $uri;

        $page_data = array();

        // --- Page methods
        $page_data = array(
            'title' => $title
        );
        // --- Page properties
        $browser_title = $title . ' - ' . CDG_APPNAME;

        $data = Array(
            'browser_title' => $browser_title,
            'title' => $title,
            'type' => $type,
            'image' => $image,
            'description' => $description,
            'url' => $url,
            'page_data' => $page_data,
        );

        return self::buildView(uri: $uri, dir:'errors', data: $data, file: 'e404.twig');
    }
    public function build403Page(string $uri) {
        $title = 'Page non autorisée';
        $type = 'website';
        $image = BASEURL . DATA . DS . 'og_image.jpg';;
        $description = "";
        $url = BASEURL . DS . $uri;

        $page_data = array();

        // --- Page methods
        $page_data = array(
            'title' => $title
        );
        // --- Page properties
        $browser_title = $title . ' - ' . CDG_APPNAME;

        $data = Array(
            'browser_title' => $browser_title,
            'title' => $title,
            'type' => $type,
            'image' => $image,
            'description' => $description,
            'url' => $url,
            'page_data' => $page_data,
        );

        return self::buildView(uri: $uri, dir:'errors', data: $data, file: 'e404.twig');
    }
    public function build500Page(string $uri) {
        $title = 'Erreur serveur';
        $type = 'website';
        $image = BASEURL . DATA . DS . 'og_image.jpg';;
        $description = "";
        $url = BASEURL . DS . $uri;

        $page_data = array();

        // --- Page methods
        $page_data = array(
            'title' => $title
        );
        // --- Page properties
        $browser_title = $title . ' - ' . CDG_APPNAME;

        $data = Array(
            'browser_title' => $browser_title,
            'title' => $title,
            'type' => $type,
            'image' => $image,
            'description' => $description,
            'url' => $url,
            'page_data' => $page_data,
        );

        return self::buildView(uri: $uri, dir:'errors', data: $data, file: 'e404.twig');
    }
}
