<?php
use Coddyger\Coddyger;
use Coddyger\cdg_directory;

class RouteHandler {
    public $uri = '';

    public function __construct(string $uri)
    {
        $this->uri = $uri;
        self::buildRoute();
    }

    public function buildRoute() {
        if(preg_match('#^/api#', $this->uri)) {
            self::apiHandler();
        } else {
            self::webHandler();
        }
    }

    public function apiHandler() {
        $uriArray = explode('/', $this->uri);
        $tmp = [];

        foreach($uriArray as $uri) {
            if(!empty($uri)) {
                array_push($tmp, $uri);
            }
        }

        $mainUri = $tmp[1];
        $path = ROOT_PATH . API . DS . $mainUri . '.api.php';

        if(file_exists($path)) {
            cdg_directory::check_file(ROOT_PATH . API, $mainUri . '.api.php');

            $str = "new ".ucfirst($mainUri)."Api();";
            eval($str);
        } else {
            $model = [
                'status' => 404,
                'message' => 'API introuvable',
                'data' => null
            ];
            Coddyger::renderJSON($model);
        }
    }
    public function webHandler() {
        $uriArray = explode('/', $this->uri);
        $tmp = [];

        foreach($uriArray as $uri) {
            if(!empty($uri)) {
                array_push($tmp, $uri);
            }
        }

        $mainUri = count($tmp) >= 1 ? $tmp[0] : 'home';

        // $path = ROOT_PATH . WEB . DS . 'web.php';
        cdg_directory::check_file(ROOT_PATH . WEB, 'web.php');
        $str = "new Web(".json_encode($mainUri).");";

        eval($str);
    }
}

