<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
use Trulyao\PhpRouter\Router as Router;
use Trulyao\PhpRouter\HTTP\Response as Response;
use Trulyao\PhpRouter\HTTP\Request as Request;

class Web
{
    private $controller;
    private $uri;
    public function __construct(string $uri)
    {
        $router = new Router(APP_DIR . DS . 'views', "");

        $this->uri = $uri;
        $this->controller = new HomeController('');

        $router->get("/", function (Request $req, Response $res) {
            return $this->controller->buildHomePage($this->uri);
        });
        $router->get("/article/:id", function (Request $req, Response $res) {
            $controller = new PostController('');
            $id = $req->params("id");

            return $controller->buildPostDetailPage($this->uri, $id);
        });
        $router->get("/about", function (Request $req, Response $res) {
            return $this->controller->buildAboutPage($this->uri);
        });

        $router->serve();
    }
}
