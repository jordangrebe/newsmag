<?php
/*
|--------------------------------------------------------------------------
| Coddyger - PHP Environment
|--------------------------------------------------------------------------
|
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
// Allow from any origin
require_once 'json.header.php';

use Coddyger\Coddyger;
use Trulyao\PhpRouter\HTTP\Response as Response;
use Trulyao\PhpRouter\HTTP\Request as Request;

class AssetsApi
{
    private $controller;
    public function __construct()
    {
        $router = API_ROUTER;

        $this->controller = new AssetsController();

        $router->get("/", function ($request, $response) {
            $request->append("time", date('d M Y'));
            return $response->use_engine()->render("Views/index.html", $request);
        });
        $router->get('/assets/select-countries', function (Request $req, Response $res) {
            $Q = $this->controller->selectCountry();
            var_dump($req);

            return Coddyger::api($res, $Q);
        });
        $router->post('/assets/post-country', function (Request $req, Response $res) {
            $Q = $this->controller->selectCountry();
            $body = $req->body();

            return Coddyger::api($res, $body);
        });

        $router->serve();
    }

    private static function canAccessApi()
    {
        $jwt = JwtMiddleware::decode();
        if ($jwt['response'] == 'success') {
            return true;
        } else {
            return false;
        }
    }
}
