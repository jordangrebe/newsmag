<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| author :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

use Coddyger\Coddyger;

class PostModel extends \Model
{
    public const table = 'app_post';
    private $userModel = null;
    private $categoryModel = null;

    function __construct()
    {
        parent::__construct(self::table);

        $this->userModel = new UserModel();
        $this->categoryModel = new CategoryModel();
    }

    /* ------------------------------------
    | CREER UN DOCUMENT
     ----------------------------------- */
    function save(object $data)
    {
        try {
            $Q = MysqlSet::run('INSERT INTO ' . self::table . ' (slug, title, content, category, uri, user) 
            VALUES (?, ?, ?, ?, ?, ?)', [$data->slug, $data->title, $data->content, $data->category, $data->uri, $data->user]);

            if ($Q) {
                return $Q;
            } else {
                throw new Exception($Q);
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | MODIFIER UN DOCUMENT
     ----------------------------------- */
    function edit(object $data)
    {
        try {
            $Q = MysqlSet::run('UPDATE ' . self::table . ' SET title = ?, content = ?, category = ?, uri = ?, updated_at = NOW() WHERE slug = ?', [$data->title, $data->content, $data->category, $data->uri, $data->slug]);

            if ($Q) {
                return $Q;
            } else {
                throw new Exception($Q);
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | SELECTIONNE DES DOCUMENTS
     ----------------------------------- */
    function select(string $category = '', int $limit = 10)
    {
        try {
            if (empty($category)) {
                $Q = MysqlSet::run('SELECT * FROM ' . self::table . ' ORDER BY created_at DESC limit ' . $limit);
            } else {
                $Q = MysqlSet::run('SELECT * FROM ' . self::table . ' WHERE category = ? AND status = ? ORDER BY created_at DESC limit ' . $limit, [$category, 'active']);
            }

            $json = [];
            $tmp = [];

            foreach ($Q as $key => $item) {
                if ($item['category'] == $category) {
                    $item['key'] = $key;
                    $tmp = $this->buildProps($item);

                    $json[$key] = $tmp;
                }
            }

            return $json;
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    function selectLatestRand(int $limit = 10)
    {
        try {
            $Q = MysqlSet::run('SELECT * FROM ' . self::table . ' ORDER BY created_at DESC limit ' . $limit);

            $json = [];
            $tmp = [];

            foreach ($Q as $key => $item) {
                $item['key'] = $key;
                $tmp = $this->buildProps($item);

                $json[$key] = $tmp;
            }

            return $json;
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | SELECTIONNE DETAILS D'UN DOCUMENT
     ----------------------------------- */
    function selectOne(string $slug)
    {
        try {
            $Q = MysqlSet::run('SELECT * FROM ' . self::table . ' WHERE slug = ? OR uri = ? OR (title = ?)', [$slug, $slug, $slug]);

            if (!$Q) {
                return false;
            } else {
                return $Q->fetch();
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /*
    |------------------
    | UTILS ---------
    |------------------
    */
    function buildProps(array $data)
    {
        $slug = Coddyger::sanitize_output($data['slug']);
        $created_at = Coddyger::sanitize_output($data['created_at']);
        $updated_at = Coddyger::sanitize_output($data['updated_at']);
        $status = Coddyger::sanitize_output($data['status']);
        $title = $data['title'];
        $uri = Coddyger::sanitize_output($data['uri']);
        $content = $data['content'];
        $resume = $data['resume'];
        $file = Coddyger::sanitize_output($data['file']);
        $category = Coddyger::sanitize_output($data['category']);
        $featured = (int)($data['featured']);
        $user = $data['user'];
        $viewed = Coddyger::sanitize_output($data['viewed']);

        $filePath = ROOT_PATH . DS . DATA . DS . 'post' . DS . $slug . DS . $file;
        $fileUri = BASEURL . DS . 'data' . DS . 'post' . DS . $slug . DS . $file;

        if (empty($file) || !file_exists($filePath)) {
            $fileUri = BASEURL . DS . 'data' . DS . 'post' . DS . 'default.png';
        }

        $post_uri = BASEURL . DS . 'article' . DS . $uri;

        $categoryData = empty($category) || $category == "undefined" ? null : $this->categoryModel->buildProps($this->categoryModel->selectOne($category));
        $userData = !empty($user) ? $this->userModel->buildProps($this->userModel->selectOne($user), foreign: true) : null;

        /*
        |------------------
        | OUTPUT ---------
        |------------------
        */
        $tmp['slug'] = $slug;
        $tmp['status'] = $status;
        $tmp['created_at'] = $created_at;
        $tmp['updated_at'] = $updated_at;

        $tmp['title'] = $title;
        $tmp['content'] = ($content);
        $tmp['resume'] = ($resume);
        $tmp['file'] = $fileUri;
        $tmp['category'] = $categoryData;
        $tmp['featured'] = $featured;
        $tmp['viewed'] = $viewed;
        $tmp['uri'] = $post_uri;
        $tmp['user'] = $userData;

        return $tmp;
    }
}
