<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

use Coddyger\Coddyger;

class CategoryModel extends \Model
{
    public static $table = 'app_category';

    function __construct() {
        parent::__construct('app_category');
    }
    /* ------------------------------------
    | SELECTIONNE DES DOCUMENTS
     ----------------------------------- */
    function select(bool $parent = false)
    {
        try {
            if($parent === true) {
                $Q = MysqlSet::run('SELECT * FROM app_category WHERE parent IS NOT NULL ORDER BY created_at DESC');
            } else {
                $Q = MysqlSet::run('SELECT * FROM app_category WHERE parent IS NULL ORDER BY created_at DESC');
            }

            return $Q->fetchAll();
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }
    static function selectByParent(string $parent)
    {
        try {
            $Q = MysqlSet::run('SELECT * FROM app_category WHERE parent = ? ORDER BY created_at DESC', [$parent]);

            return $Q->fetchAll();
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | CREER UN DOCUMENT
     ----------------------------------- */
    static function save(object $data)
    {
        try {
            $Q = MysqlSet::run('INSERT INTO app_category (slug, title, content, parent) 
            VALUES (?, ?, ?, ?)', [$data->slug, $data->title, $data->content, $data->parent]);

            if ($Q) {
                return $Q;
            } else {
                throw new Exception($Q);
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | MODIFIER UN DOCUMENT
     ----------------------------------- */
    static function edit(object $data)
    {
        try {
            $Q = MysqlSet::run('UPDATE app_category SET title = ?, content = ?, parent = ?, updated_at = NOW() WHERE slug = ?', [$data->title, $data->content, $data->parent, $data->slug]);

            if ($Q) {
                return $Q;
            } else {
                throw new Exception($Q);
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /*
    |------------------
    | UTILS ---------
    |------------------
    */
    const search_this = array('/é/','/è/','/ê/','/ë/','/à/','/ä/','/â/','/ô/','/ö/','/û/','/ü/','/î/','/ï/','/ç/');
    const replace_by = array('e','e','e','e','a','a','a','o','o','u','u','i','i','c');
    
    public static function buildProps(array $data)
    {
        $model = new CategoryModel;

        $data_ref = Coddyger::sanitize_output($data['slug']);
        $data_reg = Coddyger::sanitize_output($data['created_at']);
        $data_update = Coddyger::sanitize_output($data['updated_at']);
        $data_status = Coddyger::sanitize_output($data['status']);
        $data_name = ($data['title']);
        $data_content = Coddyger::sanitize_output($data['content']);
        $data_parent = Coddyger::sanitize_output($data['parent']);
        $category = ($data_parent == null || $data_parent == '' ? null : $model->selectOne($data_parent));
        /*
        |------------------
        | OUTPUT ---------
        |------------------
        */
        $tmp['slug'] = $data_ref;
        $tmp['status'] = $data_status;
        $tmp['created_at'] = $data_reg;
        $tmp['updated_at'] = $data_update;

        $tmp['title'] = $data_name;
        $tmp['content'] = $data_content;
        $tmp['parent'] = ($category == null ? null : self::buildProps($category));

        return $tmp;
    }
}

new CategoryModel();
