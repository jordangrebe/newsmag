<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/
class MongoDbSet extends MongoDbDatabase
{
    public static $model;

    /* ------------------------------------
    | 
     ----------------------------------- */
    public static function save(string $collection, array $query, int $limit = null)
    {
        try {
            $client = self::connect();
            $collection = $client->selectCollection(MONGODB_DATABASE, $collection);

            $documents = $collection->find($query, [
                'collation' => ['locale' => 'fr'],
                'limit' => $limit
            ]);
            
            return $documents;
        } catch (Exception $e) {
            echo 'MongoDbSet : ',  $e->getMessage(), "\n";
        }
    }

    public static function select(string $collection, array $query, int $limit = null)
    {
        try {
            $client = self::connect();
            $collection = $client->selectCollection(MONGODB_DATABASE, $collection);

            $documents = $collection->find($query, [
                'collation' => ['locale' => 'fr'],
                'limit' => $limit
            ]);
            
            return $documents;
        } catch (Exception $e) {
            echo 'MongoDbSet : ',  $e->getMessage(), "\n";
        }
    }
}
