<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

use Coddyger\cdg_database;

class MysqlSet
{
    public static $db;
    public static $model;

    /* ------------------------------------
    | CONNECTER BASE DE DONNEES
     ----------------------------------- */
    public static function mysql()
    {
        try {
            return new MysqlDatabase(host: DBHOST, pass: DBPASS, user: DBUSER, db: DBNAME);
        } catch (Exception $e) {
            echo 'MysqlSet-mysql : ',  $e->getMessage(), "\n";
        }
    }

    /* ------------------------------------
    | EXECUTE UNE REQUETE
     ----------------------------------- */
    public static function run(string $query, array $params = array())
    {
        try {
            self::$db = self::mysql()->connect();
            self::$db->query("set names utf8");

            $Q = self::$db->prepare($query);
            $Q->execute($params);

            if ($Q) {
                return $Q;
            } else {
                throw new Exception($Q);
            }
        } catch (Exception $e) {
            echo 'MysqlSet-run : ',  $e->getMessage(), "\n";
            echo 'MysqlSet-run-Query : ',  $query, "\n";
            echo 'MysqlSet-run-params : ',  json_encode($params), "\n";
        }
    }

    /* ------------------------------------
    | VERIFIE EXISTANCE D'UN DOCUMENT
     ----------------------------------- */
    public static function exist(string $table, string $column, string $key)
    {
        try {
            self::$db = self::mysql()->connect();
            self::$db->query("set names utf8");

            // Query Parameter
            $Q = self::$db->prepare('SELECT * FROM ' . $table . ' WHERE ' . $column . ' = :key');
            $Q->bindValue(':key', $key, PDO::PARAM_STR);
            $Q->execute();

            if ($Q->rowCount() >= 1) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            echo 'MysqlSet-exist : ',  $e->getMessage(), "\n";
        }
    }
}
