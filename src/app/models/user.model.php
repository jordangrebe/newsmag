<?php
/*
|--------------------------------------------------------------------------
| Coddyger
|--------------------------------------------------------------------------
|
| Version :: 1.0 : Year :: 2019.07.31
| Authors :: UltronDev
| Website :: https://www.ultrondev.com/
|
*/

use Coddyger\Coddyger;
use Coddyger\cdg_directory;

class UserModel extends \Model
{
    public const table = 'app_users';

    function __construct()
    {
        parent::__construct(self::table);
    }
    /* ------------------------------------
    | SELECTIONNE DES DOCUMENTS
     ----------------------------------- */
    function select(string $type = 'admin')
    {
        try {
            $Q = MysqlSet::run('SELECT * FROM ' . self::table . ' 
            LEFT JOIN app_admindata ON slug = app_admindata.user 
            WHERE type = ?
            ORDER BY created_at DESC', [$type]);

            return $Q->fetchAll();
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | SELECTIONNE DETAILS D'UN DOCUMENT
     ----------------------------------- */
    function selectOne(string $slug)
    {
        try {
            $Q = MysqlSet::run('SELECT * FROM ' . self::table . ' 
            LEFT JOIN app_admindata ON slug = app_admindata.user 
            WHERE slug = ? OR contact = ? OR (username = ? OR email = ?)', [$slug, $slug, $slug, $slug]);

            if (!$Q) {
                return false;
            } else {
                return $Q->fetch();
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | CREER UN DOCUMENT
     ----------------------------------- */
    function save(object $data)
    {
        try {
            $Q = MysqlSet::run('INSERT INTO ' . self::table . ' (slug, grade, type, email) 
            VALUES (?, ?, ?, ?)', [$data->slug, $data->grade, $data->type, $data->email]);

            if ($Q) {
                return $Q;
            } else {
                throw new Exception($Q);
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | MODIFIER UN DOCUMENT
     ----------------------------------- */
    function edit(object $data)
    {
        try {
            $Q = MysqlSet::run('UPDATE ' . self::table . ' SET username = ?, grade = ?, contact = ?, email = ?, updated_at = NOW() WHERE (slug = ? OR contact = ?) OR email = ?', [$data->username, $data->grade, $data->contact, $data->email, $data->slug, $data->slug, $data->slug]);

            if ($Q) {
                return $Q;
            } else {
                throw new Exception($Q);
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | MODIFIER ADRESSE E-MAIL
     ----------------------------------- */
     function editEmail(object $data)
     {
         try {
             $Q = MysqlSet::run('UPDATE ' . self::table . ' SET email = ?, updated_at = NOW() WHERE (slug = ? OR contact = ?) OR email = ?', [$data->email, $data->slug, $data->slug, $data->slug]);
 
             if ($Q) {
                 return $Q;
             } else {
                 throw new Exception($Q);
             }
         } catch (Exception $e) {
             return ['error' => true, 'data' => $e];
         }
     }

    /* ------------------------------------
    | MODIFIER UN DOCUMENT
     ----------------------------------- */
    function editAvatar(object $data)
    {
        try {
            $Q = MysqlSet::run('UPDATE ' . self::table . ' SET avatar = NULL, updated_at = NOW() WHERE (slug = ? OR contact = ?) OR email = ?', [$data->slug, $data->slug, $data->slug]);

            if ($Q) {
                return $Q;
            } else {
                throw new Exception($Q);
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | MODIFIER MOT DE PASSE
     ----------------------------------- */
    function editPassword(object $data)
    {
        try {
            $Q = MysqlSet::run('UPDATE ' . self::table . ' SET password = ?, password_level = ?, passwordRequiredEdit = ?, updated_at = NOW() WHERE (slug = ? OR contact = ?) OR email = ?', [$data->password, $data->password_level, $data->requirePasswordForEdit, $data->slug, $data->slug, $data->slug]);

            if ($Q) {
                return $Q;
            } else {
                throw new Exception($Q);
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    /* ------------------------------------
    | 
     ----------------------------------- */
    function auth(string $login)
    {
        try {
            $Q = MysqlSet::run('SELECT * FROM ' . self::table . ' WHERE (email = ? OR contact = ?) OR slug = ?', [$login, $login, $login]);

            if ($Q) {
                return $Q;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return [
                'error' => true, 'data' => $e
            ];
        }
    }

    /*
    |------------------
    | UTILS ---------
    |------------------
    */
    function buildCurrentSession($object)
    {
        $db = MysqlSet::mysql()->connect();

        $Q = $db->prepare('SELECT * FROM cdg_session WHERE session_identifier = :a ORDER BY session_reg DESC LIMIT 1');
        $Q->bindValue(':a', $object, PDO::PARAM_STR);
        $Q->execute();

        if ($Q->rowCount() >= 1) {
            $data = $Q->fetch();
            $status = Coddyger::sanitize_output($data['session_status']);

            return ($status == null || $status == '' ? '' : $status);
        } else {
            return '';
        }
    }
    function buildProps(array $data, bool $foreign = false)
    {
        $slug = Coddyger::sanitize_output($data['slug']);
        $data_reg = Coddyger::sanitize_output($data['created_at']);
        $data_update = Coddyger::sanitize_output($data['updated_at']);
        $data_status = Coddyger::sanitize_output($data['status']);
        $contact = Coddyger::sanitize_output($data['contact']);
        $email = Coddyger::sanitize_output($data['email']);
        $type = Coddyger::sanitize_output($data['type']);
        $data_password_level = (int)Coddyger::sanitize_output($data['password_level']);
        $passwordRequiredEdit = (int)Coddyger::sanitize_output($data['passwordRequiredEdit']);

        $file = Coddyger::sanitize_output($data['avatar']);

        $filePath = ROOT_PATH . DS . DATA . DS . 'user' . DS . $slug . DS . $file;
        $fileUri = BASEURL . DS . 'data' . DS . 'user' . DS . $slug . DS . $file;

        if (empty($file) || !file_exists($filePath)) {
            $fileUri = BASEURL . DS . 'data' . DS . 'user' . DS . 'default.jpg';
        }

        // 
        $firstname = Coddyger::sanitize_output($data['firstname']);
        $lastname = Coddyger::sanitize_output($data['lastname']);
        $ability = [(object)['action' => 'manage', 'subject' => 'all']];

        $tmp = [];

        if($foreign === false) {
            $tmp['contact'] = $contact;
            $tmp['email'] = $email;
            $tmp['status'] = $data_status;
            $tmp['password_level'] = (int)$data_password_level;
            $tmp['passwordRequiredEdit'] = (int)$passwordRequiredEdit;
    
            $tmp['role'] = $type;
            $tmp['created_at'] = $data_reg;
            $tmp['updated_at'] = $data_update;
            if ($type == 'admin') {
                $tmp['ability'] = $ability;
            }
        }

        $tmp['slug'] = $slug;
        $tmp['lastname'] = $lastname;
        $tmp['firstname'] = $firstname;
        $tmp['file'] = $fileUri;

        return $tmp;
    }
    function ownData(string $slug, object $payloads)
    {
        try {
            $Q = MysqlSet::run('SELECT * FROM ' . self::table . ' WHERE slug = ? AND ' . $payloads->column . ' = ?', [$slug, $payloads->data]);

            if (!$Q) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return ['error' => true, 'data' => $e];
        }
    }

    function reverseNormalizeBirthdate(string $birthdate) {
        $birthdateArray = explode('-', $birthdate);
        $year = $birthdateArray[0];
        $month = $birthdateArray[1];
        $day = $birthdateArray[2];

        return $day . '-' . $month . '-' . $year;
    }

    function normalizeBirthdate(string $birthdate) {
        $birthdateArray = explode('-', $birthdate);
        $day = $birthdateArray[0];
        $month = $birthdateArray[1];
        $year = $birthdateArray[2];

        return $year . '-' . $month . '-' . $day;
    }
}
