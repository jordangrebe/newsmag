/*
|--------------------------------------------------------------------------
| Coddyger - PHP Framework
|--------------------------------------------------------------------------
|
| Authors :: UltronDev Associates
| Website :: https://www.ultrondev.com/
|
*/

class Main {
    /*
    |---------------------------------------
    | MENU
    |---------------------------------------
    */
    static menu() {
        axios.get('/data/menu.json').then(function (response) {
            let template = '';
            let el = document.getElementById('menu');
            let currentPath = window.location.pathname;
            let isActive = '';

            
            // handle success
            let status = response.status;
            let data = response.data;

            if(status == 200) {
                let menuItems = data.landing;

                for (let x in menuItems) {
                    let item = menuItems[x];

                    let menuTitle = item.menu_title;
                    let menuUri = item.menu_link;
                    let menuSub = item.menu_sub;
        
                    if (menuUri === '/' || menuUri === '') {
                        menuUri = '/';
                    } else {
                        menuUri = '/' + menuUri;
                    }
        
                    if (menuUri === currentPath) {
                        isActive = 'active';
                    }

                    if (menuSub === '') {
                        template += '<li class="nav-item ' + isActive + '">' + '<a href="' + menuUri + '" class="nav-link text-uppercase">' + menuTitle + '</a>' + '</li>';
                    } else {
                        template += '<li class="nav-item dropdown ' + isActive + '" >' + '';
                        template += '<a id="music_sub_menu" data-toggle="dropdown" aria-expanded="false" class="nav-link dropdown-toggle text-uppercase">' + menuTitle + '</a>' + '';
                        template += '<div class="dropdown-menu dropdown-primary" aria-labelledby="music_sub_menu">';
                        for (let y in menu_sub) {
                            let menu_sub_title = menu_sub[y].menu_sub_title;
                            let menu_sub_link = menu_sub[y].menu_sub_link;
        
                            if (menu_sub_link === currentPath) {
                                isActive = 'active';
                            } else {
                                isActive = '';
                            } template += '<a class="dropdown-item text-uppercase ' + isActive + '" href="' + menu_sub_link + '"><i class="fa fa-angle-right"></i> ' + menu_sub_title + '</a>';
                        }
                        template += '</div>';
                        template += '</li>';
                    }
                }

                el.innerHTML = template;
            }
        }).catch(function (error) {
            // handle error
            console.log(error);
        });
    }

    static updateViewCount() {
        let el = document.getElementsByClassName('post-link');
        el.onclick = function() {
            alert('AAAAAAAAAAAA')
            console.log('AAAAAAAAAAAA')
        }

        return false;
        // axios.get('/post/update-view-count').then(function (response) {
        //     // handle success
        //     let status = response.status;
        //     let data = response.data;

        //     if(status == 200) {
                
        //     }
        // }).catch(function (error) {
        //     // handle error
        //     console.log(error);
        // });
    }

    static hahaha() {
        console.log('/*\n' +
        '|--------------------------------------------------------------------------\n' +
        '| Coddyger - PHP Framework\n' +
        '|--------------------------------------------------------------------------\n' +
        '|\n' +
        '| Authors :: UltronDev Associates\n' +
        '| Website :: https://www.ultrondev.com/\n' +
        '|\n' +
        '*/\n');
    }

    static init() {
        Main.menu();
        Main.updateViewCount();


        Main.hahaha();
    }
}

Main.init();
